{% set postdata = data.get('post', {}) %}

minion_key_delete:
  wheel.key.delete:
    - match: '{{ postdata.tgt }}'
